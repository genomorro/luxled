$(".fullnav").each(function() {
    var defaultItem = $(this).find(".current").first();

    $(this).find("li").hover(function() {
        defaultItem.toggleClass('current', false);
        $(this).toggleClass('current', true);
    }, function() {
        $(this).toggleClass('current', false);
        defaultItem.toggleClass('current', true);
    });
});

jQuery(function($) {
    $('.taphover').click(function() {
        return false;
    }).dblclick(function() {
        window.location = this.href;
        return false;
    });
});
